﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Customer_Template : MonoBehaviour
{
    public new string name;
    private float satisfaction = 5.5f;
    protected AIPath AI;
    public WorldController world;
    // Start is called before the first frame update
    private void Start()
    {
        AI = gameObject.GetComponent<AIPath>();
    }

    // Update is called once per frame
    public void Interact(PlayerController player)
    {
        if (player.heldItem != null)
        {
            if (PonderPricing(player.heldItem, 5))
            {
                Debug.Log("the customer will buy the item for " + player.heldItem.price);
            }
            else
            {
                Debug.Log("The item is priced too high for this customer");
            }
        }
    }

    public void RedirectPath(GameObject targetedObject)
    {
        gameObject.GetComponent<AIPath>().destination = targetedObject.transform.position;
        AI.canMove = true;
    }
    void OnPathComplete()
    {
        Debug.Log("finished the path!");
    }
    private bool PonderPricing(Item_Parent item, float proposedPrice)
    {
        float personalPrice = item.price;

        personalPrice = ((((satisfaction + world.GetReputation() + world.GetDemand(item) + world.GetSupply(item)) / 4f) / 5.5f)*item.price);

        Debug.Log(personalPrice);

        return (proposedPrice * 1.1f >= personalPrice);
    }
}
