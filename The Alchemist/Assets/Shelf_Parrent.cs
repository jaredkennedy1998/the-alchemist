﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shelf_Parrent : MonoBehaviour
{

    public DisplayController itemDisplay;
    public Item_Parent storedItem;

public bool StoreItem(Item_Parent item)
    {
        if (storedItem != null) return false;

        storedItem = item;
        item.HideItem();
        UpdateDisplay();
        return true;
    }

    public void voidItem()
    {
        storedItem = null;
        UpdateDisplay();
    }

    public void UpdateDisplay()
    {

        if (storedItem != null)
            itemDisplay.SetTexture(storedItem.getCurrentSprite());
        else
            itemDisplay.SetTexture(null);
    }
    public void DisplayUI()
    {
        Debug.Log("I should be displaying a UI right now!");
    }
}
