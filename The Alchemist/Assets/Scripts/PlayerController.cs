﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerController : MonoBehaviour
{
    Rigidbody rB;
    float moveSpeed = 50;
    public GameObject cursor, cameraParrentObject;
    public CinemachineVirtualCamera cinCam;
    string direction = "down"; //this direction is which one i am facing in
    public DisplayController itemDisplayPoint;
    Item_Parent[] storedItems = new Item_Parent[9];
    int currentItemSlot = 0;
    public Item_Parent heldItem;
    private KeyCode[] keyCodes = { KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4, KeyCode.Alpha5, KeyCode.Alpha6, KeyCode.Alpha7, KeyCode.Alpha8, KeyCode.Alpha9}; //should add a keycode for 0 later to deselect all items for when your inventory is full and no interactions are wanted.
    bool cameraMode = false; //f is cursorLerp, t is player smooth follow

    void Start()
    {
        rB = GetComponent<Rigidbody>();
        storedItems[currentItemSlot] = heldItem;
    }

    private void Update()
    {
        for(int i = 0; i < keyCodes.Length; i++)
            if (Input.GetKeyDown(keyCodes[i]))
            {
                SwapSelectedItem(i);
            }
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            cameraMode = !cameraMode;

            if (!cameraMode)//going into cursor lerp
            {
                InvokeRepeating("ClearCameraDampening", .5f,.1f);
                cinCam.m_Follow = cameraParrentObject.transform;

            }
            else //going into smooth player
            {
                cinCam.GetCinemachineComponent<CinemachineTransposer>().m_XDamping = 1;
                cinCam.GetCinemachineComponent<CinemachineTransposer>().m_ZDamping = 1;
                cinCam.m_Follow = transform;

            }
        }
        if(!cameraMode)
            cameraParrentObject.transform.position = Vector3.Lerp(transform.position, cursor.transform.position, .25f);
        
    }

    void FixedUpdate()
    {
        {
            float hAxis = Input.GetAxis("Horizontal"), vAxis = Input.GetAxis("Vertical");

            if (hAxis == 1) direction = "right";
            if (hAxis == -1) direction = "left";
            if (vAxis == 1) direction = "up";
            if (vAxis == -1) direction = "down";

            rB.AddForce(new Vector3(hAxis * moveSpeed, 0, vAxis * moveSpeed));

            UpdateItemDisplay();
        } //moves the player and updates players graphics

        {
            Vector2 mousePos = Input.mousePosition;
            Vector2 point = new Vector2((mousePos.x - (Screen.width / 2)) / 110, (mousePos.y - (Screen.height / 2)) / 110);

            cursor.transform.position = new Vector3(transform.position.x + point.x, cursor.transform.position.y, transform.position.z + point.y);
        }//moves the cursor around the screen
    }
    public void DropItem()//assumes you are dropping an item while also on its inventory slot. will have to work around this if selecting from a UI!
    {
        heldItem.RevealItem(new Vector3(cursor.transform.position.x, cursor.transform.position.y+.5f, cursor.transform.position.z));
        storedItems[currentItemSlot] = null;
        heldItem = null;
    }
    
    public bool StoreItem(Item_Parent item)
    {
        if (heldItem == item) return false; //cannot pick up an item that is currently being held

        bool itemPathDecided = false, itemStacked = false;
        int preferedSlot = 9; //9 is an invalid slot!
        int i = 0;

        while(!itemPathDecided && i <= 8)
        {
            if (storedItems[i] != null) {//check if there is an item in the slot

                itemPathDecided = (storedItems[i].AddItemToStack(item)); //tried to stack the item. returns false if it fails and does nothing. if it works it deals with the item data and returns true.
                if (itemPathDecided) //if the item was stored into another stack
                {
                    preferedSlot = i;
                    itemStacked = true;
                }
            }
            else if(preferedSlot > i && !itemPathDecided)preferedSlot = i;

            i++;
        }
        if (!itemPathDecided && preferedSlot == 9) return false;
        if(!itemStacked)
        storedItems[preferedSlot] = item;
        UpdateItemDisplay();
        if (!itemStacked)
            item.HideItem();
        else
            item.Destroy();
        Debug.Log((preferedSlot+1) + ": slot - "+storedItems[preferedSlot].name+" - count : " +storedItems[preferedSlot].stackCount);

        if (preferedSlot == currentItemSlot) SwapSelectedItem(currentItemSlot); // this will refresh the item display so that the player will handle references and sprites of the new held item. 
        return true;
    }
    private void UpdateItemDisplay()
    {
        if (heldItem != null)
            itemDisplayPoint.SetTexture(heldItem.getCurrentSprite());
        else
            itemDisplayPoint.SetTexture(null);

    }
    public Pot_Parrent GetHighlightedPot()
    {
        if(cursor.GetComponent<CursorController>().getHighlightedObject() != null)
        if (cursor.GetComponent<CursorController>().getHighlightedObject().tag.Equals("Plant"))
            return cursor.GetComponent<CursorController>().getHighlightedObject().GetComponent<Pot_Parrent>();

        return null;
    }
    public void SwapSelectedItem(int inventoryIndex)
    {
        currentItemSlot = inventoryIndex;
        heldItem = storedItems[inventoryIndex];
        UpdateItemDisplay();
    }
    public void VoidItem()
    {
        heldItem = null;
        storedItems[currentItemSlot] = null;
    }
    private void ClearCameraDampening()
    {

        cinCam.GetCinemachineComponent<CinemachineTransposer>().m_XDamping = 0;
        cinCam.GetCinemachineComponent<CinemachineTransposer>().m_ZDamping = 0;
    }



}
