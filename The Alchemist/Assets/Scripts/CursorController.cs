﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorController : MonoBehaviour
{
    public PlayerController player;
    private GameObject selectedItem;
    private bool interact = false, altInteract = false;
    public List<Collider> collidedObjects = new List<Collider>();

    private void Update()
    {
        if (Input.GetButtonDown("Interact")) { interact = true; altInteract = false; }
        if (Input.GetButtonDown("AltInteract")) { altInteract = true; interact = false; }
        if(!Input.GetButtonDown("Interact") && !Input.GetButtonDown("AltInteract")) { altInteract = false; interact = false; }
        {

            interactWithCollidables();

            if (altInteract)
                if (player.heldItem != null)
                {
                    player.DropItem();
                    interact = false;
                }
            if (interact)
            {
                if (player.heldItem != null && player.heldItem.usable) //if i am holding an item and it has a specific use
                {
                    if (player.heldItem.usable)
                        player.heldItem.UseItem(player);
                }
            }
        }
    }
    private void interactWithCollidables()
    {
        List<Collider> interactedObjects = new List<Collider>();

        for(int i = 0; i < collidedObjects.Count; i++) {
            Collider currentObject = collidedObjects[i];
            if (interact)
            {
                if (currentObject.tag.Equals("Item"))
                {
                    if (player.StoreItem(currentObject.GetComponent<Item_Parent>()))
                    collidedObjects.Remove(currentObject);


                }
                if (currentObject.tag.Equals("Plant"))
                {
                    currentObject.GetComponent<Pot_Parrent>().HarvestPlant();
                    collidedObjects.Remove(currentObject);
                }
                if (currentObject.tag.Equals("Customer"))
                {
                    currentObject.GetComponent<Customer_Template>().Interact(player);
                    collidedObjects.Remove(currentObject);
                }
                if (currentObject.tag.Equals("Shelf"))
                {
                    if (player.heldItem != null)
                        if (currentObject.GetComponent<Shelf_Parrent>().StoreItem(player.heldItem))
                        {
                            player.VoidItem();
                            collidedObjects.Remove(currentObject);
                        }
                        else
                        {
                            currentObject.GetComponent<Shelf_Parrent>().DisplayUI();
                            collidedObjects.Remove(currentObject);
                        }
                }
            }

            if (altInteract)
            {
                if (currentObject.tag.Equals("Plant"))
                {
                    //deal with plant code
                    if (currentObject.GetComponent<Pot_Parrent>().storedPlant != null)
                        if (player.StoreItem(currentObject.GetComponent<Pot_Parrent>().storedPlant))
                        {
                            currentObject.GetComponent<Pot_Parrent>().storedPlant.SetPot(null);
                            currentObject.GetComponent<Pot_Parrent>().voidPlant();
                            //interactedObjects.Add(currentObject);
                            collidedObjects.Remove(currentObject);
                        }
                }
                if (currentObject.tag.Equals("Shelf"))
                {
                    if (player.StoreItem(currentObject.GetComponent<Shelf_Parrent>().storedItem))
                    {
                        currentObject.GetComponent<Shelf_Parrent>().voidItem();
                        //interactedObjects.Add(currentObject);
                        collidedObjects.Remove(currentObject);
                    }
                }
            }
        }
    }


    private void OnTriggerEnter(Collider other) 
    {
        selectedItem = other.gameObject;
        collidedObjects.Add(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if (collidedObjects.Contains(other)) collidedObjects.Remove(other);
        if (selectedItem == other) selectedItem = null;
    }

    public GameObject getHighlightedObject()
    {
        return selectedItem;
    }
}
