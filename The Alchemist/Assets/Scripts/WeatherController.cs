﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherController : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject player,globalLighting, RainEffect;
    float gameTime = 270;
    int day = -1;
    private bool pingedDay = false, pingedPlant = false, scanned = false;
    public Light sunlight, moonlight, morningLight, settingLight;
    Plant_Parrent[] plants;
    Pot_Parrent[] pottedPlants;

    private void Start()
    {
        Cursor.visible = false;
        DayTick();
    }
    // Update is called once per frame
    void Update()
    {
        gameTime += Time.deltaTime*7.5f;
        globalLighting.transform.rotation = Quaternion.Euler((-gameTime%360), 10, transform.rotation.z);

        if ((int)(gameTime % 2) == 0 && !scanned)
        {
            AstarPath.active.Scan();
            scanned = true;
        }//udates collider pathfinding for all AI objects
        if ((int)(gameTime % 2) != 0) scanned = false;

            if (((int)(gameTime % 360)) % 5 == 0 && !pingedPlant)
        {
            pingedPlant = true;
            PlantTick();
            Invoke("resetPlantToggle",.5f);
        }
        if (((int)(gameTime % 360)) == 0 && !pingedDay)
        {
            pingedDay = true;
            DayTick();
            Invoke("resetDayToggle", 5f);

        }

        RainEffect.transform.position = new Vector3(player.transform.position.x,30,player.transform.position.z-10);


        //equation for setting light

        float minimumAngle = 300, maximumAngle = 300, lightIntensityBase = .75f, fadeRate = 30; //setting sun
        if(gameTime%360 > minimumAngle)
            settingLight.intensity= Mathf.Min(.5f, lightIntensityBase-(((gameTime%360)-maximumAngle)/ fadeRate));
        else if (gameTime % 360 < maximumAngle)
            settingLight.intensity = Mathf.Min(.5f, lightIntensityBase - ((minimumAngle - (gameTime % 360)) / fadeRate));

        minimumAngle = 60; maximumAngle = 60; lightIntensityBase = .75f; fadeRate = 30; //rising sun
        if (gameTime % 360 > minimumAngle)
            morningLight.intensity = Mathf.Min(.5f, lightIntensityBase - (((gameTime % 360) - maximumAngle) / fadeRate));
        else if (gameTime % 360 < maximumAngle)
            morningLight.intensity = Mathf.Min(.5f, lightIntensityBase - ((minimumAngle - (gameTime % 360)) / fadeRate));

        minimumAngle = 105; maximumAngle = 255; lightIntensityBase = .5f; fadeRate = 15; //moon
        if (gameTime % 360 > minimumAngle)
            moonlight.intensity = Mathf.Min(.5f, lightIntensityBase - (((gameTime % 360) - maximumAngle) / fadeRate));
        else if (gameTime % 360 < maximumAngle)
            moonlight.intensity = Mathf.Min(.5f, lightIntensityBase - ((minimumAngle - (gameTime % 360)) / fadeRate));

        if (gameTime % 360 > 180)
        {
            minimumAngle = 330; maximumAngle = 360; lightIntensityBase = .75f; fadeRate = 30; //sun right
            if (gameTime % 360 > minimumAngle)
                sunlight.intensity = Mathf.Min(.5f, lightIntensityBase - (((gameTime % 360) - maximumAngle) / fadeRate));
            else if (gameTime % 360 < maximumAngle)
                sunlight.intensity = Mathf.Min(.5f, lightIntensityBase - ((minimumAngle - (gameTime % 360)) / fadeRate));
        }
        else
        {

            minimumAngle = 0; maximumAngle = 30; lightIntensityBase = .75f; fadeRate = 30; //sun right
            if (gameTime % 360 > minimumAngle)
                sunlight.intensity = Mathf.Min(.5f, lightIntensityBase - (((gameTime % 360) - maximumAngle) / fadeRate));
            else if (gameTime % 360 < maximumAngle)
                sunlight.intensity = Mathf.Min(.5f, lightIntensityBase - ((minimumAngle - (gameTime % 360)) / fadeRate));
        }


    }

    void DayTick()
    {
        plants = Object.FindObjectsOfType<Plant_Parrent>();
        pottedPlants = Object.FindObjectsOfType<Pot_Parrent>();
        day++;
        if(day != 0)
        Debug.Log("A new day rises! we are on Day "+day+"!");
    }
    void PlantTick()
    {

        foreach (Plant_Parrent plant in plants)
        {
            plant.ponderGrowth();
        }
        foreach (Pot_Parrent pottedplant in pottedPlants)
        {
            pottedplant.ponderGrowth();
        } 
    }

    void resetPlantToggle() { pingedPlant = false;}
    void resetDayToggle() { pingedDay = false; }

}
