﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedDisplayController : DisplayController
{
    private Texture lastTexture;

    new void Start()
    {
        base.Start();
        lastTexture = texture;
        
    }

    void FixedUpdate()
    {
        if(lastTexture != texture)
        {
            lastTexture = texture;
            SetTexture(texture);
        }
    }
}
