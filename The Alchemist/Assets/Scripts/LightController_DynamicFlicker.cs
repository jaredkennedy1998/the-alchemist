﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController_DynamicFlicker : MonoBehaviour
{
    public new Light light;
    public float minimumIntensity, maximumIntensity, fadeSpeed, lightOffMin, lightOffMax,lightOnMin, lightOnMax, refreshRate = .1f;
    public Color color1, color2;


    void Start()
    {
        light.intensity = minimumIntensity;
        LerpOff();
    }


    void LerpOn()
    {

        float currentPercentage = light.intensity / maximumIntensity;

        light.intensity = Mathf.Lerp(minimumIntensity,maximumIntensity,currentPercentage + (fadeSpeed* refreshRate));
        light.color = Color.Lerp(color1, color2, currentPercentage + (fadeSpeed * refreshRate));
        light.intensity = light.intensity;
        Debug.Log(light.intensity);


        if (light.intensity == maximumIntensity)
        {
            Debug.Log("turning off");
            CancelInvoke();
            InvokeRepeating("LerpOff", Random.Range(lightOnMin, lightOnMax), refreshRate);
        }
    }

    void LerpOff()
    {
        float currentPercentage = light.intensity / maximumIntensity;

        light.intensity = Mathf.Lerp(minimumIntensity, maximumIntensity, currentPercentage - (fadeSpeed * refreshRate));
        light.color = Color.Lerp(color1, color2, currentPercentage - (fadeSpeed * refreshRate));
        light.intensity = light.intensity;
        Debug.Log(light.intensity);

        if (light.intensity == minimumIntensity)
        {
            Debug.Log("turning on");
            CancelInvoke();
            InvokeRepeating("LerpOn", Random.Range(lightOffMin, lightOffMax), refreshRate);
        }
    }
}


