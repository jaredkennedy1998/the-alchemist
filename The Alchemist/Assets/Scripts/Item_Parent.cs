﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Parent : MonoBehaviour
{
    // Start is called before the first frame update

    public DisplayController graphicalDisplay;//this is for rendering the item when it is sitting in the world. not on the player
    public new string name;
    public bool usable, stackable;
    public int stackCount = 1, stackLimit = 10;
    public float price;
    public string[] tags;



    void Update()
    {
        updateDisplay();
    }

    public virtual Texture getCurrentSprite()
    {
        return graphicalDisplay.GetTexture();
    }
    void updateDisplay()
    {
        graphicalDisplay.SetTexture(getCurrentSprite());
    }
    public virtual void UseItem(PlayerController player)
    {
        Debug.Log("you are using an item without overriding the UseItem() function in Item_Parrent!");
    }
    public void HideItem()
    {
        GetComponent<Collider>().enabled = false;
        graphicalDisplay.GetComponent<Renderer>().enabled = false;
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

        GetComponent<Rigidbody>().Sleep();

    }
    public void RevealItem(Vector3 newLocation)
    {
        transform.position = newLocation;
        GetComponent<Collider>().enabled = true;
        graphicalDisplay.GetComponent<Renderer>().enabled = true;
        GetComponent<Rigidbody>().WakeUp();

    }
    public void Destroy()
    {
        GameObject.Destroy(this.gameObject, 0);
    }
    public bool AddItemToStack(Item_Parent requestedItem)
    {
        if (!requestedItem.name.Equals(name) || requestedItem.stackCount + stackCount > stackLimit || !stackable) return false;

        stackCount += requestedItem.stackCount;
        return true;
    }
    public bool AddItemToStack(int aditionalItems)
    {
        if (stackCount + aditionalItems > stackLimit || !stackable) return false;

        stackCount += aditionalItems;
        return true;

    }
}
