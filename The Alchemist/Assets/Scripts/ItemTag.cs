﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTag : ScriptableObject
{
    protected float demand, supply;
    public  string name;
    private float ratinMin = 0, ratingMax = 10;

    public ItemTag()
    {
        demand = 5.5f;
        supply = 5.5f;
    }
    public void AdjustDemand(float adjustment)
    {
        demand += adjustment;
        if (demand > ratingMax) demand = ratingMax;
        else if (demand < ratinMin) demand = ratinMin;
    }
    public void AdjustSupply(float adjustment)
    {
        supply += adjustment;
        if (supply > ratingMax) supply = ratingMax;
        else if (supply < ratinMin) supply = ratinMin;
    }
    public float GetDemand()
    {
        return demand;
    }
    public float GetSupply()
    {
        return supply;
    }
}
