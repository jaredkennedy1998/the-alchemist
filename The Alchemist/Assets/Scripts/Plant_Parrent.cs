﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plant_Parrent : Item_Parent
{
    public Texture[] textures;
    private Pot_Parrent pot;


    //growth rate, food need, water need, light need, 
    public float averageDaysOfGrowth, dailyWaterNeed, dailyNutrientNeed;
    protected int growthCycles, growthCycle = 0, growthTick = 0, maximumGrowthTicks;
    public int[] size, harvest;
    public int postHarvestState;
    public GameObject harvestDrop;
    // Start is called before the first frame update
    protected void Start()
    {
        maximumGrowthTicks = (int)(averageDaysOfGrowth * 48);
        growthCycles = textures.Length;
        stackable = false;
        if(pot != null)
        {
            pot.updateDisplay();
        }
    }
    public void ponderGrowth()
    {
        if (pot != null)
        {
            if (growthCycles - 1 > growthCycle && pot.ponderSize(size[growthCycle])) //if the plant is not fully grown
            {
                if (pot.sapWater(dailyWaterNeed / 24) && pot.sapNutrience(dailyNutrientNeed / 24) && pot.sizeOfContainer >= size[growthCycle + 1])
                {
                    if (Random.Range(1, 24) <= 1 / averageDaysOfGrowth)
                    {
                        grow();
                    }
                    else if (growthTick >= maximumGrowthTicks)
                    {
                        grow();
                    }
                }
            }
        }
    }

    public void grow()
    {
        growthTick = 0;
        growthCycle++;
        pot.updateDisplay();
    }
    public bool ponderContainer(int sizeOfContainer) //if it can fit in a specified size of container at the curent growth cycle then return true
    {
        return sizeOfContainer >= size[growthCycle];
    }
    public override Texture getCurrentSprite()
    {
        return textures[growthCycle];
    }

    public void SetPot(Pot_Parrent pot)
    {
        this.pot = pot;
    }

    public override void UseItem(PlayerController player)
    {
        if(player.GetHighlightedPot() != null)
        {
            if (player.GetHighlightedPot().StorePlant(this))
            {
                HideItem();
                player.VoidItem();
            }

        }
    }
    public bool HarvestPlant()//bool returns of if it fails or not. instantiates harvest drops in this function. 
    {
        if (harvest[growthCycle] == 0) return false; //not able to harvest at this point

        if (harvestDrop != null)
        {
            //add in instantiation of harvest object here

            GameObject plantDrop = Instantiate(harvestDrop, new Vector3(pot.transform.position.x, pot.transform.position.y + .75f, pot.transform.position.z), pot.transform.rotation);
            plantDrop.GetComponent<Rigidbody>().AddRelativeForce(new Vector3(Random.Range(-20, 20), 20, Random.Range(-20, 20)));
            if (harvest[growthCycle] > 1) plantDrop.GetComponent<Item_Parent>().AddItemToStack(harvest[growthCycle]-1);

        }

        if (postHarvestState != 0) growthCycle = postHarvestState;
        else DestroyPlant();

        return true;
       
    }

    public new string ToString()
    {
        return name + " at cycle "+(growthCycle+1)+"/ "+growthCycles;
    }
    public void DestroyPlant()
    {
        pot.voidPlant();
        GameObject.Destroy(this.gameObject, 0);
    }
}
