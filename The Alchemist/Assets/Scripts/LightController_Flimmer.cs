﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController_Flimmer : MonoBehaviour
{
    public new Light light;
    public float minimumIntensity, maximumIntensity, lightFlickerMagnitude,lightFlickerSpeed;
    public Color color1, color2;
    private float currentIntensity;

    private void Start()
    {
        currentIntensity = (maximumIntensity - minimumIntensity) / 2;
        light.intensity = currentIntensity;
        light.color = Color.Lerp(color1, color2, currentIntensity);
        InvokeRepeating("FlimmerLight",0,lightFlickerSpeed);
    }

    void FlimmerLight()
    {
        float x = Random.Range(-lightFlickerMagnitude/2,lightFlickerMagnitude/2);
        if(currentIntensity + x >= minimumIntensity && currentIntensity + x <= maximumIntensity)
        {
            currentIntensity += x;
            light.intensity = currentIntensity;
            light.color = Color.Lerp(color1,color2,currentIntensity);
        }


    }
}
