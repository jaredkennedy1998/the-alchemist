﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldController : MonoBehaviour
{
    private List<ItemTag> itemTags = new List<ItemTag>();
    public string[] itemTagNames;

    void Start()
    {
        StartItemTags();

    }

    private void StartItemTags() //starts the creation of all of the item tags. should only be called once. 
    {
        foreach (string name in itemTagNames)
        {
            ItemTag itemTag = ScriptableObject.CreateInstance<ItemTag>();
            itemTag.name = name;
            itemTags.Add(itemTag);
        }
    }

    public void UpdateTagDemand(string name, float adjustment)
    {
        ItemTag itemTag = getItemTag(name);
        if (itemTag != null) itemTag.AdjustDemand(adjustment);
        else Debug.Log("Trying to adjust an invalid itemTag of name : " + name);
    }
    public ItemTag getItemTag(string name)
    {
        foreach(ItemTag tag in itemTags)
        {
            if (tag.name.Equals(name)) return tag;
        }
        Debug.Log("No item tag exists for: "+name);
        return null;
    }
    public float GetDemand(Item_Parent item) 
    {
        float highestDemand = 0;
        foreach (string tag in item.tags)
        {
            float tempDemand = getItemTag(tag).GetDemand();
            if (tempDemand > highestDemand) highestDemand = tempDemand;
        }
        return highestDemand;
    }
    public float GetReputation() //need to get the reputation of the player store. 
    {
        return 5.5f;
    }
    public float GetSupply(Item_Parent item) 
    {
        float lowestSupply = 10;
        foreach (string tag in item.tags)
        {
            float tempSupply = getItemTag(tag).GetSupply();
            if (tempSupply < lowestSupply) lowestSupply = tempSupply;
        }
        return lowestSupply;
    }
}


