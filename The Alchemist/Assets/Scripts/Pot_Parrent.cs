﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pot_Parrent : MonoBehaviour
{
    public Plant_Parrent storedPlant;
    public DisplayController plantDisplay;


    public int sizeOfContainer;
    public float nutrientMax, waterMax;
    public new string name;
    private float nutrientCount = 0, waterCount = 0;

    //store plant that is in the pot
    //store nutrience, water and a maximum size
    //store proper spawn point of the plant

    protected void Start()
    {

        if (storedPlant != null)
        {
            storedPlant = Instantiate(storedPlant);
            storedPlant.SetPot(this);
        }
        updateDisplay();
        //plantDisplay.GetComponent<Animator>().Play("IdlePlantSway", 0, transform.localPosition.x%1); //need to get an animation set to sway plants. this code will offset it per its coordnates to create a sway effect.
    }

    public bool StorePlant(Plant_Parrent desiredPlant) //store plant data if it can go in the pot.
    {
        if (storedPlant == null)
        {
            if (desiredPlant.ponderContainer(sizeOfContainer))
            {
                storedPlant = desiredPlant;
                storedPlant.SetPot(this);
                updateDisplay();
                return true;
            }
            else Debug.Log("that plant is too big for this container!");
        }
        else Debug.Log("there is already a plant in this container!");

        return false;
    }

    public void updateDisplay()
    {

        if (storedPlant != null)
        {
            plantDisplay.SetTexture(storedPlant.getCurrentSprite());
        }

        else
            plantDisplay.SetTexture(null);


    }

    public void ponderGrowth()

    {
        //storedPlant.ponderGrowth();
    } //this function may never be called. only necesary is certain plants are not added to the plant array in the weather controller. 

   

    public bool sapNutrience(float nutrience)
    {
        nutrientCount -= nutrience;
        if (nutrientCount >= 0) return true;

        nutrientCount = 0;
        return false;
    }

    public bool sapWater(float water)
    {
        waterCount -= water;
        if (waterCount >= 0) return true;

        waterCount = 0;
        return false;
    }

    public void addWater(float water)
    {
        waterCount += water;
        if (waterCount > waterMax) waterCount = waterMax;
    }

    public void addNutrience(float nutrience)
    {
        nutrientCount += nutrience;
        if (nutrientCount > nutrientMax) nutrientCount = nutrientMax;
    }

    public bool ponderSize(int size) { return size <= sizeOfContainer; }

    public void voidPlant()
    {
        storedPlant = null;
        updateDisplay();
    }

    public void HarvestPlant()
    {
        if(storedPlant != null)
            if (storedPlant.HarvestPlant()) updateDisplay();
    }

    public new string ToString()
    {
        if(storedPlant != null)
            return name + " storing "+storedPlant.ToString()+" \n    Nutrition at " + nutrientCount + " out of " + nutrientMax + " \n    Water at " + waterCount + " out of " + waterMax;
        else
            return name + " storing nothing.  \n    Nutrition at " + nutrientCount + " out of " + nutrientMax + " \n    Water at " + waterCount + " out of " + waterMax;

    }

}
