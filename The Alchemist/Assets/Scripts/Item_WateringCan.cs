﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_WateringCan : Item_Parent
{
    // Start is called before the first frame update
    public int wateringPower;
    ParticleSystem particles;

    private void Start()
    {
        particles = GetComponent<ParticleSystem>();
        particles.Stop();
    }
    // Update is called once per frame
    public override void UseItem(PlayerController player)
    {
        if (player.GetHighlightedPot() != null)
        {
            player.GetHighlightedPot().addWater(wateringPower);
            player.GetHighlightedPot().addNutrience(wateringPower);
        }
        gameObject.transform.position = player.cursor.GetComponent<CursorController>().transform.position;
        HideItem();
        EmitParticles(GetComponent<ParticleSystem>(),.5f);
        HideItem();


    }

    public void EmitParticles(ParticleSystem particleSettings, float emitTime)
    {
        particles.Play();
        Invoke("PauseParticleSystem", emitTime);
    }
    private void PauseParticleSystem() //used to invoke on a delay
    {
        particles.Stop();


    }
}
