﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayController : MonoBehaviour
{
    public Texture texture,normalMap;
    Material matirial;
    public Material referenceMatirial;
    private float yOffset;
    public bool accountForSpriteHeight = false;
    protected void Start()
    {
        yOffset = transform.localPosition.y;
        // matirial = new Material(Shader.Find("Standard"));
        matirial = new Material(referenceMatirial);
        matirial.SetTexture("_MainTex",texture);
        matirial.SetTexture("_BumpMap", normalMap);
        GetComponent<Renderer>().material = matirial;
        SetToPixelPerfect();

    }
    public void SetToPixelPerfect()
    {
        if (texture != null)
        {
            Vector3 scale = new Vector3(texture.width / 20f, texture.height / 20f, 1);
            transform.localScale = scale;
            if (accountForSpriteHeight)
            {
                Vector3 height = new Vector3(transform.position.x, yOffset + (texture.height / 2f / 20f) - .5f, transform.position.z);
                transform.position = (height);
            }
        }
        else
        {
            Vector3 scale = new Vector3(0, 0, 0);
            transform.localScale = scale;
        }
    }
    public void SetTexture(Texture newTexture)
    {
        if(matirial != null) 
        if (newTexture != null)
        {
            texture = newTexture;
            matirial.SetTexture("_MainTex", texture);
            SetToPixelPerfect();
        }
        else
        {
            texture = null;
            matirial.SetTexture("_MainTex", null);
            SetToPixelPerfect();
        }

    }
    public void SetTexture(Texture newTexture, Texture normalMap)
    {
        if (normalMap != null){
            texture = normalMap;
            matirial.SetTexture("_BumpMap", texture);
        }
        SetTexture(newTexture);
    }
    public Texture GetTexture()
    {
        return texture;
    }
   
}
